#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:83886080:1e3a5da9bade770dcf8dbbfa02740609c7146085; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:be29926bb0d510c50c6ecebe98c195ac38b03a46 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:83886080:1e3a5da9bade770dcf8dbbfa02740609c7146085 && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
